package ru.energizet.pks30.recicler.adapter

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ru.energizet.pks30.recicler.databinding.AdapterListBinding
import java.util.*

class ListAdapter(var items: ArrayList<Item>) :
    RecyclerView.Adapter<ListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val L = AdapterListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(L)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.L.tvTitle.text =
            String.format(Locale.US, "%s Оценочка %d из 10", item.title, item.num)
        holder.L.tvText.text = item.text
        if (item.image != null) {
            holder.L.ivImage.visibility = View.VISIBLE
            holder.L.ivImage.setImageDrawable(item.image)
        } else {
            holder.L.ivImage.visibility = View.GONE
        }
        holder.L.tvText.post {
            val layoutParams = holder.L.ivImage.layoutParams
            layoutParams.height = holder.L.tvText.height + holder.L.tvTitle.height
            holder.L.ivImage.layoutParams = layoutParams
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class ViewHolder(val L: AdapterListBinding) :
        RecyclerView.ViewHolder(L.root)

    class Item(
        val title: String,
        val text: String,
        val num: Int,
        val image: Drawable?
    )
}