package ru.energizet.pks30.recicler.activity

import android.graphics.drawable.Drawable
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import ru.energizet.pks30.recicler.R
import ru.energizet.pks30.recicler.adapter.ListAdapter
import ru.energizet.pks30.recicler.databinding.ActivityMainBinding
import ru.energizet.pks30.recicler.lists.LeftList
import ru.energizet.pks30.recicler.lists.RightList

class MainActivity : AppCompatActivity() {

    companion object {
        enum class Text(val string: String) {
            LEFT("Пушка"),
            RIGHT("Агонь")
        }
    }

    private var L: ActivityMainBinding? = null
    private var images: ArrayList<Drawable?> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        L = ActivityMainBinding.inflate(layoutInflater)
        setContentView(L!!.root)

        images.add(ContextCompat.getDrawable(this, R.drawable.hdm))
        images.add(ContextCompat.getDrawable(this, R.drawable.f2))
        images.add(ContextCompat.getDrawable(this, R.drawable.sm))

        L!!.tvLeft.text = Text.LEFT.string
        L!!.tvRight.text = Text.RIGHT.string
        L!!.tvLeft.setOnClickListener { tvLeftOnClick() }
        L!!.tvRight.setOnClickListener { tvRightOnClick() }
        initRecycleView()
    }

    private fun initRecycleView() {
        L!!.rvList.layoutManager = LinearLayoutManager(this)
        L!!.tvHead.text = Text.LEFT.string
        L!!.rvList.adapter = ListAdapter(LeftList.getList(images))
    }

    private fun tvLeftOnClick() {
        L!!.tvHead.text = Text.LEFT.string
        (L!!.rvList.adapter as ListAdapter).items = LeftList.getList(images)
        L!!.rvList.adapter?.notifyDataSetChanged()
    }

    private fun tvRightOnClick() {
        L!!.tvHead.text = Text.RIGHT.string
        (L!!.rvList.adapter as ListAdapter).items = RightList.getList(images)
        L!!.rvList.adapter?.notifyDataSetChanged()
    }
}