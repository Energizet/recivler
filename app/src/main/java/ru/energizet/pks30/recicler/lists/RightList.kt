package ru.energizet.pks30.recicler.lists

import android.graphics.drawable.Drawable
import ru.energizet.pks30.recicler.adapter.ListAdapter

class RightList {
    companion object {
        fun getList(images: ArrayList<Drawable?>): ArrayList<ListAdapter.Item> {
            val list = ArrayList<ListAdapter.Item>()
            for (i in 1..15) {
                list.add(
                    ListAdapter.Item(
                        "Фильм $i",
                        "Описание\nФильм $i - агонь",
                        i % 3 + 8,
                        if (images.size > i % 3) images[i % 3] else null
                    )
                )
            }
            return list
        }
    }
}